# Updog Banking

<img src="https://gitlab.com/hiimkai/updog-banking/-/raw/main/utils/demo-images/1.png">

What is Updog? Glad you asked, it's a fake bank to troll refund scammers. It is free, open source and licensed under the GPL. It is packed with features to annoy refund scammers and waste their time. 

## How to Use
1. Git clone ```git clone --depth=1 https://gitlab.com/hiimkai/updog-banking```
2. Create a Python virtual environment
3. Install the requirements ```cd utils; pip3 install -r requirements.txt```
4. ```python run.py```

It should run on ```localhost:5000```. All configuration can be found in ```core/conf.py```.

Note: Don't use Updog Bank in prod unless you know what you're doing. It's not secure from hackers and won't display properly on a mobile device. Updog bank is meant to be used on your local server for refund scams.

## Features
- Basic online bank functionality
- Fully functional authentication. You can have multiple users with multiple accounts and freely edit data about each account.
- Fake ads. *"Flat earth"* recruitment, *"join the Mario party"*
- Annoying captcha system. A correct response is any number under 10.
- A "this site accepts cookies" popup. If you accept, it asks you where you want to mail the cookies to
- A visit from Clippy, here to tell you random cat facts
- The option to set an invisible overlay on the profile page, making it harder to edit the HTML
- The option to set a white overlay on the page that slowly moves down, making the website look like it's loading extremely slow.
- The option to auto-refresh the page every 10 seconds, making it impossible for the scammer to edit the HTML
- The option to quickly log-out signed in accounts for "sus activity"
- Adding / removing users, accounts and transactions is all configurable in an easy, graphical GUI.

## How to configure
Global config can be found in the conf.py file. What everything does is explained there.
To configure new users, accounts and account transactions, go to the footer and click on "What's Updog?" and it will open the configuration panel.

## Demo Images
<img src="https://gitlab.com/hiimkai/updog-banking/-/raw/main/utils/demo-images/1.png">
<img src="https://gitlab.com/hiimkai/updog-banking/-/raw/main/utils/demo-images/2.png">
<img src="https://gitlab.com/hiimkai/updog-banking/-/raw/main/utils/demo-images/3.png">

<hr />

(c) Kai

<img src="https://www.gnu.org/graphics/gplv3-or-later.png" />
