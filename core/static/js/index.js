const popup = document.querySelector(".popup");
const popupCloseButton = document.querySelector(".popup-close-button");

function showPopup() {
    if (localStorage.getItem("show-popup") != "false" && localStorage.getItem("show-welcome") == "false") {
        popup.style.pointerEvents = "none";
        popup.style.animation = "fadeIn 10s ease-in forwards";
        popup.style.display = "block";
    
        function allowPopupPointerEvents() {
            popup.style.pointerEvents = "all";
        };
    
        setTimeout(allowPopupPointerEvents, 10000);
    };
};

function typingEffect() {
	const slogan = document.querySelector(".slogan");
	let sloganText = slogan.textContent.split(" ");
	slogan.textContent = "";
	let intervalPosition = 0;
	
	setInterval(() => {
		if (sloganText[intervalPosition] == undefined) {
			clearInterval();
		} else {
      slogan.style.animation = "fadeIn 1s ease-in forwards"
			slogan.textContent += sloganText[intervalPosition];
			slogan.textContent += " ";
			intervalPosition++;
		};
	}, 125);
};

function hidePopup() {
    localStorage.setItem("show-popup", "false");
    popup.style.display = "none";
};

setTimeout(showPopup, 5000);
typingEffect();

function addWelcomeCookie() {
  localStorage.setItem("show-welcome", "false");
  let welcome = document.querySelector("#welcomeArea");
  welcome.style.display = 'none';
};

if (localStorage.getItem("show-welcome") != "false") {
  let welcome = document.querySelector("#welcomeArea");
  welcome.style.display = 'block';
};

popupCloseButton.addEventListener("click", hidePopup);

document.addEventListener('DOMContentLoaded', function() {
  new Swiper("#testimonials", {
    slidesPerView: 1,
    spaceBetween: 30,
    loop: true,
    navigation: {
      nextEl: ".testimonial-swiper-button-next",
      prevEl: ".testimonial-swiper-button-prev",
    },
  });

  new Swiper("#welcome-swiper", {
    effect: "coverflow",
    grabCursor: true,
    centeredSlides: true,
    slidesPerView: "1",
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: false,
    },

    pagination: {
      el: ".swiper-pagination",
      type: "fraction"
    },
  });
});
