from core import db
from flask_login import UserMixin
from .com import Utils
from .conf import MAX_NUM_LENGTH

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    uid = db.Column(db.String(24), nullable=False)
    created = db.Column(db.DateTime, nullable=False, default=Utils.CommonFunctions.getDbTime())

    name = db.Column(db.String(100), nullable=False, unique=True)
    email = db.Column(db.String(256), nullable=False, unique=True)
    password = db.Column(db.String(512), nullable=False)
    account_number = db.Column(db.Integer, nullable=False, unique=True)
    routing_number = db.Column(db.Integer, nullable=False, unique=True)
    balance = db.Column(db.String(MAX_NUM_LENGTH), nullable=False)
    
    account = db.relationship("Account", backref="User", lazy="dynamic")

    def __repr__(self):
        return self.uid

class Account(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    uid = db.Column(db.String(24), nullable=False)
    created = db.Column(db.DateTime(timezone=True), nullable=False, default=Utils.CommonFunctions.getDbTime())

    name = db.Column(db.String(100), nullable=False)
    balance = db.Column(db.String(MAX_NUM_LENGTH), nullable=False, default=0)

    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    transactions = db.relationship("Transactions", backref="Account", lazy="dynamic")
    
    def __repr__(self):
        return self.name


class Transactions(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    uid = db.Column(db.String(24), nullable=False)
    created = db.Column(db.DateTime(timezone=True), nullable=False, default=Utils.CommonFunctions.getDbTime())

    name = db.Column(db.String(100), nullable=False)
    transaction_type = db.Column(db.String(100), nullable=False)
    amount = db.Column(db.String(MAX_NUM_LENGTH), nullable=False)
    account_id = db.Column(db.Integer, db.ForeignKey('account.id'))

    def __repr__(self):
        return self.name