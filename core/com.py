from random import choice
from pyisemail import is_email
from PIL import Image, ImageOps
from colorama import Fore, Style
from sqlalchemy.sql import func
import requests
from .conf import MAX_NUM_AMOUNT, MAX_NUM_LENGTH

class Utils:
    slogans = [
        "Give Your Credit Score A Round Of A-paws!", 
        "Smells' Like Updog, Looks Like Savings!", 
        "Trusted By Millions Since Our Founding In Kenya.",
        "Now Contains Improved Fraud Prevention.",
        "Give Your Funds More Bank Than Bark.",
        "The Bank That Doesn't Hound You With High Fees.",
        "Forget Putting Your Funds On A Short Leash.",
        "The Online Bank With Reliability Though The Ruff!",
        "Cranking transfer speeds up to level K9!",
        "Fetch yourself a bank that you can rely on.", 
    ]

    class CommonFunctions:
        def genUid():
            output = ""
            chars = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890"

            for i in range(24):
                output += choice(chars)

            return output

        def valSignup(email, name, password1, password2, account_number, routing_number):
            output = {}
            from .models import User
            # AHHHHHHHHHHHHHHHHHHHHH
            if len(email) <= 256 and len(name) <= 100 and len(password1) <= 512 and len(password2) <= 512 and account_number <= 99999999999999999 and routing_number <= 999999999:
                if len(email) > 3 and len(name) > 0 and len(password1) > 3 and len(password2) > 3 and account_number > 100000000 and routing_number > 100000000:
                    if password1 == password2:
                        if is_email(email, allow_gtld=True, check_dns=False):
                            if User.query.filter_by(email=email).first() == None: 
                                if type(account_number) == int and type(routing_number) == int:
                                    if bool(User.query.filter_by(routing_number=routing_number).first()) == False and bool(User.query.filter_by(account_number=account_number).first()) == False:
                                        output['valid'] = True
                                    else:
                                        output["flashed"] = "A user already has that account number or routing number."
                                else:
                                    output['flashed'] = "Account or routing number is too high"
                            else:
                                output['flashed'] = "The user already exists."
                        else:
                            output['flashed'] = "The email is not valid."
                    else:
                        output['flashed'] = "The passwords are not equal."
                else:
                    output['flashed'] = "Too few characters in one of the input fields."
            else:
                output['flashed'] = "Too many characters in one of the input fields."
    
            return output

        def valLogin(email, password):
            output = {}
            from .models import User

            user = User.query.filter_by(email=email).first()

            if user != None:
                if user.password == password:
                    output["valid"] = True
                    output["flashed"] = "You are now logged in!"
                else:
                    output['flashed'] = "Wrong password."
            else:
                output["flashed"] = "User does not exist."
            
            return output

        def valAccountAction(name, balance):
            output = {}

            if len(name) <= 128 and len(balance) <= MAX_NUM_LENGTH:
                if len(name) > 3 and len(balance) > 1:
                    output["valid"] = True
                else:
                    output["flashed"] = "Name or balance is too short."
            
            else:
                output["flashed"] = "Name or balance is too high."

            return output

        def valTransactionAction(name, amount, transaction_type, transaction_account_id):
            output = {}
            
            if len(name) <= 128 and len(amount) <= MAX_NUM_LENGTH:
                if len(name) > 3:
                    if len(amount) > 1:
                        if transaction_type in ["deposit", "withdrawal"]:
                            output["valid"] = True
                        else:
                            output["flashed"] = "Invalid Transaction Type"
                    else:
                        output["flashed"] = "Amount is too short."
                else:
                    output["flashed"] = "Name is too short."
            else:
                output["flashed"] = "Name or amount is too long."

            return output

        def generateTestimonials():
            for i in range(3):
                img = requests.get("https://thispersondoesnotexist.com/image", stream=True)

                if img.status_code == 200:
                    image_dir = f"core/static/img/testimonials/{i}.jpg"
                    
                    with open(image_dir, "wb") as f:
                        f.write(img.content)
                        img_edit = Image.open(image_dir)
                        img_edit = img_edit.resize((128, 128))
                        img_edit = ImageOps.grayscale(img_edit)
                        img_edit.save(image_dir)
                        # then, compress the image
                        print(Fore.GREEN + "Wrote image to testominals.")
                        print(Style.RESET_ALL)
            
            return True

        def genCensoredNumbers(account_number, routing_number):
            account_num = str(account_number)[0:3]
            routing_num = str(routing_number)[0:3]
            account_num_len = len(str(account_number)) - 3
            routing_num_len = len(str(routing_number)) - 3

            account_num_stars = ""
            routing_num_stars = ""

            for i in range(account_num_len):
                account_num_stars += "*"
            
            for i in range(routing_num_len):
                routing_num_stars += "*"

            return {"account_num": account_num, "routing_num": routing_num, "account_num_stars": account_num_stars,"routing_num_stars": routing_num_stars}
        
        # have to do this because otherwise sqlalchemy just uses the same defaults over and over again.
        def getDbTime():
            return func.now()
